$(document).ready(function(){
    $('.slider__slides').slick({
        arrows: false,
        dots: true
    });
    $('.slider__arrow--prev').click(function () {
        $('.slider__slides').slick('slickPrev');
    });
    $('.slider__arrow--next').click(function () {
        $('.slider__slides').slick('slickNext');
    });

    // pop show
    $('.carousel__slides').slick({
        arrows: false,
        // dots: true,
        infinite: true,
        centerMode: true,
        slidesPerRow: 3,
        slidesToShow: 3,
        initialSlide: 0,
        centerPadding: 0
    });
    $('.carousel__arrow--prev').click(function () {
        $('.carousel__slides').slick('slickPrev');
    });
    $('.carousel__arrow--next').click(function () {
        $('.carousel__slides').slick('slickNext');
    });
});