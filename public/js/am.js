$(document).ready(function () {
   'use strict';

   var h = 150;
   var el = document.getElementsByClassName('am')[0];

   window.onscroll = function () {
       var scroll = window.pageYOffset || document.documentElement.scrollTop;

       if ((scroll >= h && !(el.classList.contains('active'))) || (scroll < h && el.classList.contains('active'))) {
           el.classList.toggle('active');
       }
   }
});